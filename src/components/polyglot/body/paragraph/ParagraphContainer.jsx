import React, {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";
import axios from "axios";
import {apiUrl, axiosConfig} from "../../../../config";
import MyButton from "../../../ui/MyButton";
import MySelect from "../../../ui/MySelect";
import MyMonacoEditor from "../../../ui/MyMonacoEditor";
import Output from "../Output";
import {dataDivider, dataSplitter} from "../../../../data";
import {clearOutputCode, runParagraph, toggleCodeCode, toggleOutputCode} from "../../button_codes";

function ParagraphContainer(props) {
    const defaultLanguageFromHeader = document.getElementById("paragraph-language-dropdown").value;
    const [selectedLanguage, setSelectedLanguage] = useState(defaultLanguageFromHeader);
    const [toggleInput, setToggleInput] = useState(true)
    const [showOutput, setShowOutput] = useState(false)
    const [error, setError] = useState({exception: false, message: ""})
    const [status, setStatus] = useState("")
    const [info, setInfo] = useState({hide: true, info: ""})
    const [isClear, setIsClear] = useState(false)
    const [output, setOutput] = useState({status: -1, result: {variables: [], constants: [], objects: []}})
    const { keycloak } = useKeycloak();
    const [code, setCode] = useState("");

    useEffect(() => {
        try {
            // параграф с данными
            if (props.paragraphdata != dataDivider) {
                let data = props.paragraphdata.split(dataSplitter)
                let lang = data[0]
                let code = data[1]
                let output = data[2]
                setCode(code)
                setOutput(output)
                setSelectedLanguage(lang)
            }
        } catch (ex) {
        }
    }, []);

    const getVariables = (vars) => {
        let variables = []
        for (let index in vars) {
            const name = vars[index].name
            const type = vars[index].type
            const value = vars[index].value
            variables.push({name: name, type: type, value: value})
        }
        return variables
    }

    const run = async() => {
        try {
            setStatus("running")
            let requestData = {
                language: selectedLanguage,
                code: code
            }
            axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
            let resp = await axios.post(
                `${apiUrl}/notebook/${props.notebookid}/paragraph/${props.id}`,
                requestData,
                axiosConfig
            )
            let respData = await resp.data
            let outputData = {variables: [], constants: [], objects: []}

            outputData.variables = getVariables(respData.variables)
            outputData.constants = getVariables(respData.constants)
            outputData.objects = getVariables(respData.objects)

            setOutput({status: resp.status, result: outputData});
            setError({exception: false, message: ""})
            setStatus("")
        } catch (error) {
            setStatus("error")
            setError({
                exception: true,
                message: error.message + "(" + error.response.data + ")"
            })
        } finally {
            setShowOutput(true);
            setIsClear(false);
            setInfo({hide: false,
                info: String(new Date().toLocaleString("en-US",
                    { timeZone: "Europe/Moscow" }))}
            );
        }
    }

    const onChangeLanguage = (lang) => {
        setSelectedLanguage(lang)
        setCode("")
    }

    return (
        <div className={'paragraph-container ' + selectedLanguage + ' ' + status}>
            <div className="paragraph-input">
                <div className="paragraph-input-tools">
                    <MyButton
                        className="run-paragraph icon-button"
                        onClick={run}
                        title="Run this paragraph (only)"
                        viewBox="0 0 448 512"
                        alt="Run"
                        path={runParagraph}
                    />
                    <div className="paragraph-label">{props.id}</div>
                    <div className="lang-selector">
                        <MySelect
                            key={props.id}
                            selectedValue={selectedLanguage}
                            onChange={onChangeLanguage}
                            options={[
                                {value: "python", name: "Python"},
                                {value: "kotlin", name: "Kotlin"},
                                {value: "java", name: "Java"},
                                {value: "scala", name: "Scala"},
                                {value: "javascript", name: "Javascript"}
                            ]}
                        />
                    </div>
                    {!info.hide && <div className="exec-info output">
                        <span className="exec-start"> {info.info} </span>
                        <span className="exec-duration"> </span>
                    </div>}
                    <div className="options">
                        <MyButton
                            className="toggle-code icon-button"
                            onClick={() => setToggleInput(!toggleInput)}
                            title="Show/Hide CodeHeader"
                            viewBox="0 0 16 16"
                            alt="Run"
                            path={toggleCodeCode}
                        />

                        <MyButton
                            className="toggle-output icon-button"
                            onClick={() => setShowOutput(!showOutput)}
                            title="Show/Hide Output"
                            viewBox="0 0 1024 1024"
                            alt="Run"
                            path={toggleOutputCode}
                        />

                        <MyButton
                            className="clear-output icon-button"
                            onClick={() => {
                                setShowOutput(false)
                                setIsClear(true)
                                setStatus("")
                                setInfo({hide: true,
                                    info: String("")})
                            }}
                            title="Clear Output"
                            viewBox="0 0 16 16"
                            alt="Run"
                            path={clearOutputCode}
                        />

                    </div>
                </div>
                {toggleInput &&
                    <div className="paragraph-input-editor">
                        <MyMonacoEditor
                            lang={selectedLanguage}
                            setCode={c => setCode(c)}
                            code={code}
                        />
                    </div>
                }
            </div>

            {showOutput &&
                <Output output={output}
                        error={error}
                        isClear={isClear}
                />
            }
        </div>
    );
}

export default ParagraphContainer;