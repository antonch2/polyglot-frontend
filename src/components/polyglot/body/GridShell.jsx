import React from "react";
import Kernel from "./Kernel";
import NotebooksTree from "./NotebooksTree";

// Левые и правые части ноутбука: Дерево директории и Ядро
function GridShell(props) {

    return (
        <div className={'grid-shell ' + props.side}
             style={{ gridArea: props.area, width: props.width }}>
            {(function() {
                    if (props.side === "left") {
                        return (
                            <NotebooksTree
                                notebookid={props.notebookid}
                                setNotebookid={props.setNotebookid}
                                notebooks={props.notebooks}
                                setNotebooks={props.setNotebooks}
                                paragraphs={props.paragraphs}
                                setParagraphs={props.setParagraphs}
                                kernelTasks={props.kernelTasks}
                                addKernelTask={props.addKernelTask}
                            />
                        )
                    } else {
                        return <Kernel notebookid={props.notebookid}
                                       kernelTasks={props.kernelTasks}
                                       setKernelTasks={props.setKernelTasks}
                                       addKernelTask={props.addKernelTask}
                                       removeKernelTask={props.removeKernelTask}/>
                    }
                })()
            }
        </div>
    );
}

export default GridShell;