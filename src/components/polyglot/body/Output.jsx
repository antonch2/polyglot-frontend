import React from "react";

function Output(props) {
    return (
        <div className="paragraph-output">
            <div className="paragraph-output-margin"/>
            <div className="paragraph-output-block">
                <div className="paragraph-output-container">
                    <div className={"paragraph-output-display " + props.status}/>
                    {(function() {
                            if (props.error.exception === false) {
                                return <DisplayOutput
                                    output={props.output}
                                    isClear={props.isClear}/>
                            } else if (props.status === "error") {
                                return <ErrorOutput
                                    error={props.error}
                                    isClear={props.isClear}/>
                            } else {
                                return <div/>
                            }
                        })()
                    }
                </div>
            </div>
            <div className="paragraph-result-margin"/>
            <div className="paragraph-output-tools">
                <div className="result-tabs"/>
            </div>
        </div>
    );
}

function ContextVariable(props) {
    return (
        <div className="context-variable">
            <h3 className="var">
                name={props.name}, type={props.type}, value={props.value}
            </h3>
        </div>
    );
}

function ContextOutput(props) {
    return (
        <div className="context-output">
            {props.result.variables.map((variable, index) => (
                <ContextVariable
                    key={index}
                    name={variable.name}
                    type={variable.type}
                    value={variable.value}/>
            ))}
            {props.result.constants.map((constant, index) => (
                <ContextVariable
                    key={index}
                    name={constant.name}
                    type={constant.type}
                    value={constant.value}/>
            ))}
            {props.result.objects.map((obj, index) => (
                <ContextVariable
                    key={index}
                    name={obj.name}
                    type={obj.type}
                    value={obj.value}/>
            ))}
        </div>
    );
}

const DisplayOutput = (props) => {
    const style = {
        color: "#9f5454"
    }
    return (
        <div className="output" rel="stdout" style={style}>
            {
                props.isClear ? "" :
                    (<ContextOutput result={props.output.result}/>)
            }
        </div>
    );
}

function ErrorOutput(props) {
    const style = {
        color: "#d51818"
    }
    return (
        <div className="errors" style={style}>
            <blockquote className="error-report Error">
                <details className="">
                    <summary className="">
                        <span className="severity">
                            {props.isClear ? "" : props.error.message}
                        </span>
                    </summary>
                    <ul className="stack-trace">
                        <li className="">
                            <span className="error-link">(Line) </span>
                        </li>
                    </ul>
                </details>
            </blockquote>
        </div>
    )
}

export default Output;