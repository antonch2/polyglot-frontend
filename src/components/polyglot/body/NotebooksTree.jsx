import React, {useState} from "react";
import MyButton from "../../ui/MyButton";
import {Button, Modal} from "antd";
import TreeView from "./TreeView";
import {apiUrl, axiosConfig} from "../../../config";
import axios from "axios";
import {useKeycloak} from "@react-keycloak/web";
import {defaultNotebooks} from "../../../data";
import {getFreeId} from "../utils";
import {createNewNotebookIconButton} from "../button_codes";

function NotebooksTree(props) {
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const { keycloak } = useKeycloak();

    const showModal = () => {
        setOpen(true);
    };

    const splitViewComponent = document.querySelector(".split-view")
    const [collapsed, setCollapsed] = useState(false)

    const collapse = (e) => {
        if (!e.target.className.toString().includes("button") && !(e.target.className instanceof SVGAnimatedString)) {
            setCollapsed(!collapsed)
            if (collapsed) {
                splitViewComponent.classList.remove("left-collapsed")
            } else {
                splitViewComponent.classList.add("left-collapsed")
            }
        }
    }

    const onNotebookCreated = async() => {
        setLoading(true);
        let error = false
        axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
        let title = document.querySelector(".notebook-modal .ant-modal-body .modal-txt").value
        await axios.post(`${apiUrl}/notebook/`, {title: title}, axiosConfig)
            .then(res => {})
            .catch(err => {
                error = true
                let task = {}
                let id = String(getFreeId(props.kernelTasks))
                task[id] = "Failed to create paragraph: " + err
                props.addKernelTask(task)
            })
        if (!error) {
            await refreshNotebooks(axiosConfig)
            setTimeout(() => {
                setLoading(false);
                setOpen(false);
            }, 3000);
        }
        setTimeout(() => {
            setLoading(false);
            setOpen(false);
        }, 1);
    };

    const refreshNotebooks = async(axiosConfig) => {
        axiosConfig.headers["Authorization"] = `Bearer ${keycloak.token}`
        await axios.get(`${apiUrl}/notebook/all`, axiosConfig)
            .then(response => {
                const dataNotebooks = response.data
                let notebooks = {}
                for (let notebookIndex in dataNotebooks) {
                    let id = dataNotebooks[notebookIndex].id
                    notebooks[id] = dataNotebooks[notebookIndex].title
                }
                props.setNotebooks(notebooks)
            })
            .catch(() => {
                props.setNotebooks(defaultNotebooks)
            })
    }

    const handleCancel = () => {
        setOpen(false);
    };

    return (
        <div className="ui-panel">
            <h2 className="ui-panel-header notebooks-list-header" onClick={collapse}>
                Notebooks
                <span className="buttons">
                    <MyButton
                        className="create-notebook icon-button"
                        onClick={showModal}
                        title="Create new notebook"
                        viewBox="0 0 512 512"
                        alt="Create notebook"
                        path={createNewNotebookIconButton}
                    />
                </span>
            </h2>
            <div className="ui-panel-content left">
                <div className="notebooks-list">
                    <TreeView notebookid={props.notebookid}
                              setNotebookid={props.setNotebookid}
                              notebooks={props.notebooks}
                              setNotebooks={props.setNotebooks}
                              paragraphs={props.paragraphs}
                              setParagraphs={props.setParagraphs}
                              kernelTasks={props.kernelTasks}
                              addKernelTask={props.addKernelTask}
                    />
                </div>
            </div>
            <Modal
                open={open}
                className="notebook-modal"
                title="Create notebook"
                onCancel={handleCancel}
                footer={[
                    <div className="buttons">
                        <Button key="cancel-btn" onClick={handleCancel}>
                            Cancel
                        </Button>
                        <Button key="create-btn" type="primary" loading={loading} onClick={onNotebookCreated}>
                            Create
                        </Button>
                    </div>
                ]}
            >
                <input className="modal-txt" type="text" placeholder=""/>
            </Modal>
        </div>
    );
}

export default NotebooksTree;