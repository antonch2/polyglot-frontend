import React from "react";

import GridShell from "./GridShell";
import DragHandle from "./DragHandle";
import TabView from "./TabView";
import {dragHandleLeftStyle, dragHandleRightStyle, leftShellStyle, rightShellStyle} from "../polyglot_styles";

function Body(props) {

    return (
        <div className="split-view">
            <GridShell notebookid={props.notebookid}
                       setNotebookid={props.setNotebookid}
                       notebooks={props.notebooks}
                       setNotebooks={props.setNotebooks}
                       paragraphs={props.paragraphs}
                       setParagraphs={props.setParagraphs}

                       kernelTasks={props.kernelTasks}
                       setKernelTasks={props.setKernelTasks}
                       addKernelTask={props.addKernelTask}
                       removeKernelTask={props.removeKernelTask}
                       side="left" area={leftShellStyle.gridArea} width={"145px"}/>
            <DragHandle side="left" width={"145px"} area={dragHandleLeftStyle.area}/>
            <TabView notebookid={props.notebookid}
                     setNotebooks={props.setNotebooks}
                     paragraphs={props.paragraphs}
                     createParagraph={props.createParagraph}
                     insertParagraph={props.insertParagraph}
                     setParagraphs={props.setParagraphs}

                     kernelTasks={props.kernelTasks}
                     addKernelTask={props.addKernelTask}
            />
            <DragHandle side="right" width={"300px"} area={dragHandleRightStyle.area}/>
            <GridShell notebookid={props.notebookid}
                       setNotebookid={props.setNotebookid}
                       notebooks={props.notebooks}
                       setNotebooks={props.setNotebooks}
                       paragraphs={props.paragraphs}
                       setParagraphs={props.setParagraphs}

                       kernelTasks={props.kernelTasks}
                       setKernelTasks={props.setKernelTasks}
                       addKernelTask={props.addKernelTask}
                       removeKernelTask={props.removeKernelTask}
                       side="right" area={rightShellStyle.gridArea} width={"200px"}/>
        </div>
    );
}

export default Body;