import React from "react";
import MyButton from "../../ui/MyButton";
import {multipleRunCode, runCode, stopCode} from "../button_codes";

function CodeHeader() {
    return <div className="toolbar code">
        <h3> Code </h3>
        <div className="tool-group">

            <MyButton
                className="run-cell icon-button"
                title="Run this cell (only)"
                viewBox="0 0 448 512"
                alt="Run"
                path={runCode}
            />

            <MyButton
                className="run-cell to-cursor icon-button"
                title="Run all cells above, then this cell"
                viewBox="0 0 512 512"
                alt="Run to cursor"
                path={multipleRunCode}
            />

            <MyButton
                className="stop-cell icon-button"
                title="Stop/cancel this cell"
                viewBox="0 0 448 512"
                alt="Cancel"
                path={stopCode}
            />

        </div>
    </div>
}

export default CodeHeader;