import React from "react";

import NotebookHeader from "./NotebookHeader";
import ParagraphHeader from "./ParagraphHeader";
import CodeHeader from "./CodeHeader";
import AboutHeader from "./AboutHeader";

function Header(props) {
    return <div className="header">
            <div className="toolbar-container">
                <NotebookHeader
                    notebookid={props.notebookid}
                    notebooks={props.notebooks}
                    paragraphs={props.paragraphs}
                />
                <ParagraphHeader notebookid={props.notebookid}
                                 paragraphs={props.paragraphs}
                                 deleteParagraph={props.deleteParagraph}
                                 insertParagraph={props.insertParagraph}

                                 kernelTasks={props.kernelTasks}
                                 addKernelTask={props.addKernelTask}
                />
                <CodeHeader/>
                <AboutHeader
                    saveNotebook={props.saveNotebook}
                />
            </div>
        </div>
}

export default Header;