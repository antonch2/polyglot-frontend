import React from "react";

const MySelect = ({children, options, selectedValue, onChange, ...props}) => {
    return (
        <select {...props}
                style={{ outline: "none" }}
                value={selectedValue}
                onChange={event => onChange(event.target.value)}>
            {options.map(option =>
                <option key={option.value}
                        value={option.value}> {option.name} </option>
            )}
            {children}
        </select>
    )
}

export default MySelect;