.PHONY: build

start:
	npm start -Y

build-keycloak:
	docker run --name auth -p 8080:8080 -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_USER=admin -e DB_VENDOR=H2 jboss/keycloak

build-react-app:
	cd src && npm install
	npm start -Y


.DEFAULT_GOAL := start